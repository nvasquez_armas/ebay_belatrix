package pages;

import org.openqa.selenium.WebDriver;

public class BasePage {
    private WebDriver driver;
    private EbayPage ebayPage;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public EbayPage getEbayPage(){

        return (ebayPage == null) ? ebayPage = new EbayPage(driver) : ebayPage;

    }
}
