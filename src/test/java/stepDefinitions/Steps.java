package stepDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.chrome.ChromeDriver;
import pages.BasePage;
import pages.EbayPage;

import java.util.concurrent.TimeUnit;

public class Steps {
    WebDriver driver;
    BasePage basePage;
    EbayPage ebayPage;

    @Given("^enter to Ebay")
    public void i_enter_to_ebay() throws Throwable {
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        basePage = new BasePage(driver);
        ebayPage = basePage.getEbayPage();        
        assertTrue("No fue posible acceder a 'ebay.com'", ebayPage.navegar_en_ebay());
    }

    @When("^cuando ingreso el texto \"([^\"]*)\" en el textbox de busqueda$")
    public void i_enter_text(String text) throws Throwable {
    	ebayPage.ingresar_texto_en_textbox(text);
//    	assertTrue("No fue posible acceder a 'ebay.com'", ebayPage.ingresar_texto_en_textbox(text));
    }

    @And("^hago click en el boton \"([^\"]*)\"$")
    public void i_click_button(String text) throws Throwable {
    	ebayPage.click_en_btn(text);
    }

    @Then("^se muestra el listado de productos filtrados$")
    public void show_list_products() throws Throwable {
    	ebayPage.wait_load_page();
    }

    @When("^cuando selecciono el checkbox con el texto \"([^\"]*)\"$")
    public void i_click_checkbox(String text) throws Throwable {
    	if(text.equals("PUMA"))
    		ebayPage.click_en_checkbox_marca(text);
    	if(text.equals("10"))
    		ebayPage.click_en_checkbox_talla(text);
    }

    @And("^imprimo el numero de resultados$")
    public void print_console_number_result() throws Throwable {
        ebayPage.print_cant_resultado();
    }

    @When("^selecciono la opcion de ordenamiento \"([^\"]*)\"$")
    public void i_choose_sort_by(String text) throws Throwable {
        ebayPage.ordenamiento_por(text);
    }

    @When("^assert the order taking the first \"([^\"]*)\" results$")
    public void i_assert_order_taking_the_first(String text) throws Throwable {
        ebayPage.selecciono_productos(text);
    }

    @Then("^el carrito muestra los productos seleccionados$")
    public void se_muestran_productos_seleccionados() {
        ebayPage.click_en_carrito();
    }

    @And("^show the products with their prices and print them in console$")
    public void print_console_products_with_prices() throws Throwable {
        ebayPage.print_console_products();
    }

    @And("^order and print the products by name in ascendant mode$")
    public void print_console_products_by_name_ascendant() throws Throwable {
    	ebayPage.imprimo_en_consola_productos_ascendente_por_nombre();
    }

    @And("^order and print the products by price in descendant mode$")
    public void print_console_products_by_price_descendant() throws Throwable {
        ebayPage.imprimo_en_consola_productos_descendente_por_precio();
    }

    @And("^envio reporte por email$")
    public void envio_reporte_por_email() throws Throwable {
        ebayPage.sendMail();
    }

    @And("^genera reporte$")
    public void genera_reporte() throws Throwable {
        ebayPage.generarReporte();
    }
    
    @After
    public void tearDown() {
      driver.quit();
    }

}
