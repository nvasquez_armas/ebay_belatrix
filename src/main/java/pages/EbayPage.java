package pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.InfoCompra;
import pages.SendMail;
import pages.GenerarReporte;;

public class EbayPage {
    WebDriver driver;
    Actions action;
    WebDriverWait wait;
    List<InfoCompra> listaCompras;
	InfoCompra compra;
	SendMail sendMail;
	GenerarReporte generarReporte;
	
    public EbayPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        action = new Actions(driver);
        wait = new WebDriverWait(driver, 60);
        
        listaCompras = new ArrayList<InfoCompra>();
    }

    @FindBy(xpath = "//input[@placeholder='Buscar artículos']")
    private WebElement txt_search;

    @FindBy(xpath = "//input[@value='Buscar']")
    private WebElement btn_search;
    
    @FindBy(xpath = "//*[@id='w3-w13']/ul/li/div/a/div/div/span[contains(text(),'PUMA')]")
    private WebElement chk_marca;
  
    @FindBy(xpath = "//ul/li/div/a/div/div/span[contains(text(),'10')]")
    private WebElement chk_talla;
    
    @FindBy(xpath = "//*[@id='mainContent']/div[1]/div/div[2]/div/div[1]/h1")
    private WebElement lbl_result;
  
    @FindBy(xpath = "//button/div/div[contains(text(),'Mejor resultado')]")
    private WebElement btn_order;    
  
    @FindBy(xpath = "//div/div/ul/li[4]/a/span[contains(text(),'Precio + Envío: más bajo primero')]")
    private WebElement opc_order;
    
    @FindBy(xpath = "//select[@name='Größe']")
    private WebElement opc_talla1;
    
    @FindBy(xpath = "//select[@name='US Size']")
    private WebElement opc_talla2;
    
    @FindBy(xpath = "//*[@id='isCartBtn_btn']")
    private WebElement btn_agregarCarrito;
      
    @FindBy(xpath = "//select[@name='gh-cart']")
    private WebElement btn_carrito;
    
    
    public boolean navegar_en_ebay() {
        driver.get("http://www.ebay.com");
        return true;
    }

    public void ingresar_texto_en_textbox(String texto) {
        txt_search.sendKeys(texto);
    }

    public void click_en_btn(String texto) {
        btn_search.click();
    }
    
    public void click_en_carrito() {
        //btn_carrito.click();
    }

    public void click_en_checkbox_marca(String texto) {
    	chk_marca.click();
    }
    
    public void click_en_checkbox_talla(String texto) {
    	chk_talla.click();
    }

    public void print_cant_resultado() {
        System.out.println(lbl_result.getText());
    }

    public void ordenamiento_por(String texto) {
    	action.moveToElement(btn_order).perform();
		opc_order.click();
    }

    public void selecciono_productos(String texto) throws InterruptedException {
    	for(int i=1; i<=5; i++){    		
    		add_prod_a_lista(i);    		
    	    WebElement itemList = driver.findElement(By.xpath("//*[@id='srp-river-results']/ul/li[" + i + "]"));
    	    itemList.click();
    	    try {
	    	    //if(opc_talla1.isDisplayed()) 
	    	    	opc_talla1.sendKeys("43");
    	    }catch(Exception e) {
    	    	try {
    	    		opc_talla2.sendKeys("US 10 / UK 9 / 280mm");
    	    	}catch(Exception e2) {
    	    		//
    	    	}
    	    }
    	    btn_agregarCarrito.click();
    	    muestra_vista_carrito_compras();
    	    driver.navigate().back();
    	    driver.navigate().back();
    	}
    }
    
    private void add_prod_a_lista(int i) {
		compra = new InfoCompra();
		
		compra.setNameProd(driver.findElement(By.xpath("//*[@id='srp-river-results']/ul/li[" + i + "]/div/div[2]/a/h3")).getText());
		compra.setPriceProd(driver.findElement(By.xpath("//*[@id='srp-river-results']/ul/li[" + i + "]/div/div[2]/div[3]/div[1]/span")).getText());
		listaCompras.add(compra);
    }
    
    public void print_console_products() {
    	for (InfoCompra element : listaCompras) {
    		System.out.println(element.getNameProd());
    		System.out.println(element.getPriceProd());
    	}
    }
    
    private void muestra_vista_carrito_compras() {
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='mainContent']/div/div[1]/h1[contains(text(),'Carro de compras')]")));
    }

    public void imprimo_en_consola_productos_ascendente_por_nombre() {
    	System.out.println("= = = = = = = = = = = ORDER BY NAME ASC = = = = = = = = = = = = = = = = = =");
    	listaCompras.sort(Comparator.comparing(e -> e.getNameProd()));
    	print_console_products();
    }

    public void imprimo_en_consola_productos_descendente_por_precio() {
    	System.out.println("= = = = = = = = = = = ORDER BY PRICE DESC = = = = = = = = = = = = = = = = = ");
    	Comparator<InfoCompra> comparator = Comparator.comparing(e -> e.getPriceProd());
    	listaCompras.sort(comparator.reversed());
   	
    	print_console_products();
    }
    
    public void wait_load_page() throws InterruptedException {
    	Thread.sleep(1500);
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body")));
    }
    
    public void sendMail() {
    	String destinatario =  "nvasquez.armas@gmail.com"; //usaremos como destinatario al mismo remitente.
        String asunto = "Reporte ebay";
        String cuerpo = "Reporte de ejecución automatizada 'ebay'";
        sendMail.enviarConGMail(destinatario, asunto, cuerpo);
    }
    
    public void generarReporte() {
    	generarReporte.creaReporte(listaCompras);
    }
}
