package pages;

import java.io.File;
import java.io.FileWriter;
import java.util.List;
import pages.InfoCompra;

public class GenerarReporte {
	public static boolean creaReporte(List<InfoCompra> listaCompras) {
		try {
			String strPath = System.getProperty("user.dir") + "\\output\\";

			File pathDir = new File(strPath);
			pathDir.mkdirs();
			File archivo = new File(strPath + "reporte.html");
			archivo.delete(); //elimina el archivo si ya existe
			FileWriter escribir = new FileWriter(archivo, true);
			
			for (InfoCompra element : listaCompras) {
				escribir.write(element.getNameProd() + "<BR>");
				escribir.write(element.getPriceProd() + "<BR>");
			}			
			escribir.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return true;
	}
}
