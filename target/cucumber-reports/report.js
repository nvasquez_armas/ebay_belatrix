$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("realizar_una_compra.feature");
formatter.feature({
  "line": 1,
  "name": "realizar_una_compra",
  "description": "",
  "id": "realizar-una-compra",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "realizar una compra en Ebay",
  "description": "",
  "id": "realizar-una-compra;realizar-una-compra-en-ebay",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "enter to Ebay",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "cuando ingreso el texto \"shoes\" en el textbox de busqueda",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "hago click en el boton \"Buscar\"",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "se muestra el listado de productos filtrados",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "cuando selecciono el checkbox con el texto \"PUMA\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "se muestra el listado de productos filtrados",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "cuando selecciono el checkbox con el texto \"10\"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "se muestra el listado de productos filtrados",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "imprimo el numero de resultados",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "selecciono la opcion de ordenamiento \"Precio + Env�o: m�s bajo primero\"",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "se muestra el listado de productos filtrados",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "assert the order taking the first \"5\" results",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "el carrito muestra los productos seleccionados",
  "keyword": "Then "
});
formatter.step({
  "line": 17,
  "name": "show the products with their prices and print them in console",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "order and print the products by name in ascendant mode",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "order and print the products by price in descendant mode",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "genera reporte",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.i_enter_to_ebay()"
});
formatter.result({
  "duration": 7554972211,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "shoes",
      "offset": 25
    }
  ],
  "location": "Steps.i_enter_text(String)"
});
formatter.result({
  "duration": 109465567,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Buscar",
      "offset": 24
    }
  ],
  "location": "Steps.i_click_button(String)"
});
formatter.result({
  "duration": 4228137599,
  "status": "passed"
});
formatter.match({
  "location": "Steps.show_list_products()"
});
formatter.result({
  "duration": 1547685743,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PUMA",
      "offset": 44
    }
  ],
  "location": "Steps.i_click_checkbox(String)"
});
formatter.result({
  "duration": 1688527832,
  "status": "passed"
});
formatter.match({
  "location": "Steps.show_list_products()"
});
formatter.result({
  "duration": 1551432646,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 44
    }
  ],
  "location": "Steps.i_click_checkbox(String)"
});
formatter.result({
  "duration": 1724677727,
  "status": "passed"
});
formatter.match({
  "location": "Steps.show_list_products()"
});
formatter.result({
  "duration": 1530068316,
  "status": "passed"
});
formatter.match({
  "location": "Steps.print_console_number_result()"
});
formatter.result({
  "duration": 34482722,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Precio + Env�o: m�s bajo primero",
      "offset": 38
    }
  ],
  "location": "Steps.i_choose_sort_by(String)"
});
formatter.result({
  "duration": 1646669431,
  "status": "passed"
});
formatter.match({
  "location": "Steps.show_list_products()"
});
formatter.result({
  "duration": 1543295315,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 35
    }
  ],
  "location": "Steps.i_assert_order_taking_the_first(String)"
});
formatter.result({
  "duration": 91090660881,
  "status": "passed"
});
formatter.match({
  "location": "Steps.se_muestran_productos_seleccionados()"
});
formatter.result({
  "duration": 113543,
  "status": "passed"
});
formatter.match({
  "location": "Steps.print_console_products_with_prices()"
});
formatter.result({
  "duration": 439713,
  "status": "passed"
});
formatter.match({
  "location": "Steps.print_console_products_by_name_ascendant()"
});
formatter.result({
  "duration": 1094521,
  "status": "passed"
});
formatter.match({
  "location": "Steps.print_console_products_by_price_descendant()"
});
formatter.result({
  "duration": 959470,
  "status": "passed"
});
formatter.match({
  "location": "Steps.genera_reporte()"
});
formatter.result({
  "duration": 3669327,
  "status": "passed"
});
});