Feature: realizar_una_compra

  Scenario: realizar una compra en Ebay
    Given enter to Ebay
    When cuando ingreso el texto "shoes" en el textbox de busqueda
    And  hago click en el boton "Buscar"
    Then se muestra el listado de productos filtrados
    When cuando selecciono el checkbox con el texto "PUMA"
    Then se muestra el listado de productos filtrados
    When cuando selecciono el checkbox con el texto "10"
    Then se muestra el listado de productos filtrados
    And  imprimo el numero de resultados
    When selecciono la opcion de ordenamiento "Precio + Env�o: m�s bajo primero"
    Then se muestra el listado de productos filtrados
    When assert the order taking the first "5" results
    Then el carrito muestra los productos seleccionados
    And  show the products with their prices and print them in console
    And  order and print the products by name in ascendant mode
    And  order and print the products by price in descendant mode
    And  genera reporte
    #And  envio reporte por email
